import tqdm
import tensorflow as tf
from callbacks import (
    CosineDecayLearningRateScehuler,
    EarlyStopping)
from evaluators import Evaluator


class SupervisedTrainer():

    def __init__(self):
        pass

    @tf.function
    def train_step(
            self,
            model,
            optimizer,
            X_sup,
            y_sup,
            supervised_loss,
            training_params):
        # Compute loss and gradients
        with tf.GradientTape() as tape:
            # Compute loss
            y_pred = model(X_sup, training=True)

            sup_loss = supervised_loss(
                y_sup, y_pred)
            total_loss = sup_loss

            grads = tape.gradient(
                total_loss, model.trainable_variables)

            # Update student model weights
            optimizer.apply_gradients(
                zip(grads, model.trainable_variables))

        return total_loss, grads, y_pred

    def train(
            self,
            model,
            train_supervised_dataset,
            val_supervised_dataset,
            training_params,
            validation_params):

        # Initialize metrics
        train_total_loss = tf.keras.metrics.Mean(name="train_total_loss")
        train_acc = tf.keras.metrics.SparseCategoricalAccuracy("train_acc")

        # Initialize losses
        supervised_loss = tf.keras.losses.SparseCategoricalCrossentropy(
            from_logits=False)

        # Define optimizer
        optimizer = tf.keras.optimizers.Adam(
            lr=training_params["LEARNING_RATE"])

        # Create callbacks
        cosine_decay_lr_scheduler = CosineDecayLearningRateScehuler(
            optimizer=optimizer,
            lr_init=training_params["LEARNING_RATE"],
            lr_final=training_params["MIN_LEARNING_RATE"],
            decay_steps=training_params["COSINE_DECAY_LR_STEPS"])
        early_stopping = EarlyStopping(
            model=model,
            patience=training_params["PATIENCE_EARLY_STOPPING"])

        for epoch in range(0, training_params["EPOCHS"]):

            pbar = tqdm.tqdm(
                train_supervised_dataset.dataset,
                unit="batch",
                desc="Epoch {}/{}, tot_loss: {}, acc: {}".format(
                    epoch+1,
                    training_params["EPOCHS"],
                    None, None),
                total=train_supervised_dataset.num_batches)

            # Training for one epoch
            for X_sup, y_sup in pbar:

                total_loss, grads, y_pred_teacher = self.train_step(
                    model=model,
                    optimizer=optimizer,
                    X_sup=X_sup,
                    y_sup=y_sup,
                    supervised_loss=supervised_loss,
                    training_params=training_params
                )

                train_total_loss.update_state(total_loss)
                train_acc.update_state(y_sup, y_pred_teacher)
                pbar.set_description(
                    "Epoch {}/{}, loss: {}, acc: {}".format(
                        epoch+1,
                        training_params["EPOCHS"],
                        train_total_loss.result(),
                        train_acc.result()))

            # Validation step
            evaluator = Evaluator()
            val_loss, val_acc = evaluator.evaluate(
                model,
                val_supervised_dataset,
                validation_params,
                return_gt_preds=False)
            print("Val loss: {}, Val acc: {}".format(
                val_loss,
                val_acc))

            cosine_decay_lr_scheduler.on_epoch_end(epoch+1)
            early_stopping.on_epoch_end(val_loss)
            if early_stopping.stop_training:
                break
