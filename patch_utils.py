import tensorflow as tf
import numpy as np
import itertools


def generate_permutations(num_permutations):
    """ Generates `num_permutations` of the set (0,1,2,3,4,5,6,7,8)
        that represents the patch index in a 3 X 3 grid :
            [[0, 1, 2],
            [3, 4, 5],
            [6, 7, 8]]

    Args:
        num_permutations (int): Number of indexes permutations to generate.

    Returns:
        list: List containing `num_permutations` permutations of the set
            (0,1,2,3,4,5,6,7,8)
    """
    perms = list(itertools.permutations(np.arange(0, 9), r=None))
    sampled_indexes = np.random.choice(
        np.arange(len(perms)), num_permutations, replace=False)
    sampled_permutations = [perms[sampled_index]
                            for sampled_index in sampled_indexes]
    return sampled_permutations


class JigsawPuzzle():
    """Class implementing the Jigsaw puzzle transformation. Given a specified
        set of permutations and an image, it sample a permutation, shuffle a 3 X 3
        grid of patches from this image and return the shuffled grid of patches
        with the index of the permutation associated to it.


    Args:
        patch_height (int): Jigsaw patch height.
        patch_width (int): Jigsaw patch width.
        patch_gap (int): Gap between jigsaw patches.
        patch_jitter_border (int): Jittering allowed for jigsaw patches.
        permutations (list): List of possible patch index permutations.
    """

    def __init__(
            self,
            patch_height,
            patch_width,
            patch_gap,
            patch_jitter_border,
            permutations):

        self.patch_height = patch_height
        self.patch_width = patch_width
        self.patch_gap = patch_gap
        self.patch_jitter_border = patch_jitter_border
        self.grid_cell_height = self.patch_height + \
            self.patch_jitter_border+2*self.patch_gap
        self.grid_cell_width = self.patch_width + \
            self.patch_jitter_border+2*self.patch_gap
        self.grid_height = 3*self.grid_cell_height
        self.grid_width = 3*self.grid_cell_width
        self.permutations = tf.convert_to_tensor(permutations, dtype=tf.int32)

    def __call__(self, x):
        # Get a random crop where the 3 X 3 grid of patches is going to be
        # extracted
        grid = tf.image.random_crop(
            x, size=[self.grid_height, self.grid_width, 3])

        x_offset = tf.random.uniform(
            (9,), minval=0, maxval=self.patch_jitter_border, dtype=tf.int32)
        y_offset = tf.random.uniform(
            (9,), minval=0, maxval=self.patch_jitter_border, dtype=tf.int32)
        x_tlc, y_tlc = tf.meshgrid(
            tf.range(self.patch_gap, self.grid_width,
                     delta=self.grid_cell_width),
            tf.range(self.patch_gap, self.grid_height,
                     delta=self.grid_cell_height)
        )

        x_offset = tf.cast(x_offset, tf.float32)
        y_offset = tf.cast(y_offset, tf.float32)
        x_tlc = tf.cast(x_tlc, tf.float32)
        y_tlc = tf.cast(y_tlc, tf.float32)

        x_tlc = (tf.reshape(x_tlc, [-1])+x_offset)
        y_tlc = (tf.reshape(y_tlc, [-1])+y_offset)
        x_brc = (x_tlc+self.patch_width)
        y_brc = (y_tlc+self.patch_height)
        boxes = tf.stack(
            [y_tlc, x_tlc, y_brc, x_brc], axis=-1)/tf.convert_to_tensor(
            [self.grid_height, self.grid_width, self.grid_height, self.grid_width],
            dtype=tf.float32)
        box_indices = tf.zeros(9, dtype=tf.int32)

        # Extract the patches
        patches = tf.image.crop_and_resize(
            image=tf.expand_dims(grid, axis=0),
            boxes=boxes,
            box_indices=box_indices,
            crop_size=[self.patch_height, self.patch_width])

        # Sample a permutation among all the possible permutations
        permutation_index = tf.random.uniform(
            (1,), minval=0, maxval=len(self.permutations), dtype=tf.int32)
        sampled_permutation = tf.squeeze(
            tf.gather(self.permutations, permutation_index))

        # Shuffle the patch according the permutation
        patches = tf.gather(patches, sampled_permutation)

        return patches, tf.cast(permutation_index, tf.float32)
