# coding: utf-8

# Define the data dir. Data should be organized as follow :
#
# data_dir
# ├──img1.jpeg
# ├──img2.jpeg
# └──img3.jpeg
#
# If it is not the case, modify load_data.py
DATA_DIR = "/data/all_fgcs"
