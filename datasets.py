import numpy as np
import tensorflow as tf
from patch_utils import JigsawPuzzle


class JigsawPuzzleDataset():
    """This class implements a Tensorflow Dataset to generate a jigsaw puzzle of
        images.

    Args:
        img_files (list): List of images path.
        patch_height (int): Jigsaw patch height.
        patch_width (int): Jigsaw patch width.
        patch_gap (int): Gap between jigsaw patches.
        patch_jitter_border (int): Jittering allowed for jigsaw patches.
        permutations (list): List of possible patch index permutations.
        batch_size (int): Batch size for training.
        resize_shape (tuple): Resize shape for preprocessing.
        scaling (float): Scaling parameter for preprocessing.
        num_steps_per_epoch (int): Number of steps per epoch.
        shuffle (bool): Boolean indicating whether or not shuffling the data
            between each epoch.

    Attributes:
        jigsaw_puzzle (patch_utils.JigsawPuzzle):
            JigsawPuzzle object that given an image x, generates a random
            jigsaw puzzle and the patch index permutation associated to it.
        dataset (tf.data.Dataset): Dataset object generating jigsaw puzzle data
        num_batches (int): Total number of batches for training for one epoch.
        img_files (list): List of images path.
        batch_size (int): Batch size for training.
        resize_shape (tuple): Resize shape for preprocessing.
        scaling (float): Scaling parameter for preprocessing.
        num_steps_per_epoch (int): Number of steps per epoch.
        shuffle (bool): Boolean indicating whether or not shuffling the data
            between each epoch.

    """

    def __init__(
            self,
            img_files,
            patch_height,
            patch_width,
            patch_gap,
            patch_jitter_border,
            permutations,
            batch_size=32,
            resize_shape=None,
            scaling=None,
            num_steps_per_epoch=None,
            shuffle=False):
        super(JigsawPuzzleDataset, self).__init__()
        self.img_files = img_files
        self.jigsaw_puzzle = JigsawPuzzle(
            patch_height=patch_height,
            patch_width=patch_width,
            patch_gap=patch_gap,
            patch_jitter_border=patch_jitter_border,
            permutations=permutations)
        self.batch_size = batch_size
        self.resize_shape = resize_shape
        self.scaling = scaling
        self.num_steps_per_epoch = num_steps_per_epoch
        self.shuffle = shuffle
        self.dataset = self.build_dataset()
        if self.num_steps_per_epoch is not None:
            self.num_batches = num_steps_per_epoch
        else:
            self.num_batches = np.floor(len(img_files)/batch_size).astype(int)

    def _generator(self):
        for i in range(len(self.img_files)):
            yield self.img_files[i]

    def read_and_preprocess(self, filename):
        img = tf.io.read_file(filename)
        img = tf.image.decode_jpeg(img)
        img = tf.cast(img, tf.float32)
        if self.scaling is not None:
            img = img*self.scaling
        if self.resize_shape is not None:
            img = tf.image.resize(
                img, [self.resize_shape[1], self.resize_shape[0]])
        return img

    def build_dataset(self):

        dataset = tf.data.Dataset.from_generator(
            generator=self._generator,
            output_types=(tf.dtypes.string))

        # Read data and preprocess raw images
        dataset = dataset.map(
            lambda x: (self.read_and_preprocess(x)),
            num_parallel_calls=tf.data.experimental.AUTOTUNE)

        # Get a jigsaw puzzle from raw images and the permutation associated
        # to the puzzle.
        dataset = dataset.map(
            lambda x: (self.jigsaw_puzzle(x)),
            num_parallel_calls=tf.data.experimental.AUTOTUNE)

        dataset = dataset.repeat(1)
        if self.shuffle:
            dataset = dataset.shuffle(
                buffer_size=5000,
                reshuffle_each_iteration=True)

        dataset = dataset.batch(
            self.batch_size,
            drop_remainder=True)

        dataset = dataset.prefetch(tf.data.experimental.AUTOTUNE)
        if self.num_steps_per_epoch is not None:
            dataset = dataset.take(self.num_steps_per_epoch)

        return dataset
