# Unsupervised Learning of Visual Representations by Solving Jigsaw Puzzles

This project implements the self-supervised framework detailed in this
<a href="https://arxiv.org/pdf/1603.09246.pdf">paper</a>.

<p align="center">
<img src="https://davidstutz.de/wordpress/wp-content/uploads/2017/03/noroozi.png" alt="Jigsaw puzzle of an image" scale="1"/>
</p>

## Get started

### Set up environment

#### Via conda

1. Create the conda environment

```bash
conda create -n jigsaw_puzzle python=3.7 \
		tqdm \
		tensorflow \
		scikit-learn
```

2. Activate the conda environment

```bash
conda activate jigsaw_puzzle
```

3. For GPUs users only

```bash
conda install -c anaconda tensorflow-gpu
```

### Set configurations

The data dir should be specified in `constants.py`.
<br>
The configuration can be modified through the file `config.py`.

### Train a model to solve the Jigsaw puzzle task

After activating the environment and setting all the configurations in `constants.py` and `config.py`. Run :
```bash
python main.py
```

### Transfer learning from a model trained on the jigsaw puzzle task

At the end of the training with SimCLR framework, the encoder weights are saved and the head weights discarded. Now, if we want to use the encoder part for transfer learning, we just need to recreate a base_model and load the weights like the following :

```python
import models

# Creating the base model and remove the top fully connected
pretrained_model = models.ResNet18(
    classes=5,
    include_top=False)
# Build the model and load pretrained weights
pretrained_model.build((None, None, None, 3))
pretrained_model.load_weights("./checkpoints/my_model_name.h5")
```
