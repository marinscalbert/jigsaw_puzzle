import os
import constants
import config
import load_data
import patch_utils
import datasets
import models
import trainers


def main():
    # Load img files
    train_img_files, val_img_files = load_data.load_img_files(
        data_dir=constants.DATA_DIR,
        train_size=config.TRAIN_SIZE,
        val_size=config.VAL_SIZE)

    # Generate possible num_permutations
    permutations = patch_utils.generate_permutations(config.NUM_PERMUTATIONS)

    # Create dataset and sanity check on the dataset output_types
    train_jigsaw_puzzle_dataset = datasets.JigsawPuzzleDataset(
        img_files=train_img_files,
        patch_height=config.PATCH_HEIGHT,
        patch_width=config.PATCH_WIDTH,
        patch_gap=config.PATCH_GAP,
        patch_jitter_border=config.PATCH_JITTER_BORDER,
        permutations=permutations,
        batch_size=config.BATCH_SIZE,
        resize_shape=config.RESIZE_SHAPE,
        scaling=config.SCALING,
        num_steps_per_epoch=config.NUM_STEPS_PER_EPOCH,
        shuffle=config.SHUFFLE)

    val_jigsaw_puzzle_dataset = datasets.JigsawPuzzleDataset(
        img_files=val_img_files,
        patch_height=config.PATCH_HEIGHT,
        patch_width=config.PATCH_WIDTH,
        patch_gap=config.PATCH_GAP,
        patch_jitter_border=config.PATCH_JITTER_BORDER,
        permutations=permutations,
        batch_size=config.BATCH_SIZE,
        resize_shape=config.RESIZE_SHAPE,
        scaling=config.SCALING,
        num_steps_per_epoch=config.NUM_BATCHES_FOR_EVALUATION,
        shuffle=False)

    # Create model
    base_model_class = models.get_model_class(config.MODEL_NAME)
    resnet_jigsaw = models.ResNetJigsawPuzzle(
        resnet_class=base_model_class,
        num_permutations=config.NUM_PERMUTATIONS)

    # Train the model
    trainer = trainers.SupervisedTrainer()
    trainer.train(
        model=resnet_jigsaw,
        train_supervised_dataset=train_jigsaw_puzzle_dataset,
        val_supervised_dataset=val_jigsaw_puzzle_dataset,
        training_params={
            "EPOCHS": config.EPOCHS,
            "LEARNING_RATE": config.LEARNING_RATE,
            "MIN_LEARNING_RATE": config.MIN_LEARNING_RATE,
            "COSINE_DECAY_LR_STEPS": config.COSINE_DECAY_LR_STEPS,
            "PATIENCE_EARLY_STOPPING": config.PATIENCE_EARLY_STOPPING
        },
        validation_params={})

    # Save base_model
    print("Dumping trained weights")
    os.makedirs("./checkpoints", exist_ok=True)
    resnet_jigsaw.base_model.save_weights(
        "./checkpoints/{}.h5".format(config.MODEL_CHECKPOINT_NAME))

    # Create another model and load weights for sanity check
    print("Creating and building other model")
    pretrained_model = base_model_class(
        classes=config.NUM_PERMUTATIONS,
        include_top=False)
    pretrained_model.build((None, 128, 128, 3))
    print("Loading dumped weights")
    pretrained_model.load_weights(
        "./checkpoints/{}.h5".format(config.MODEL_CHECKPOINT_NAME))


if __name__ == '__main__':
    main()
