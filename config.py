# Configuration for split
TRAIN_SIZE = 0.9
VAL_SIZE = 0.1

# Configuration for model
MODEL_NAME = "ResNet18"

# Configuration for jigsaw puzzle
NUM_PERMUTATIONS = 1000
PATCH_HEIGHT = 32
PATCH_WIDTH = 32
PATCH_GAP = 2
PATCH_JITTER_BORDER = 5

# Configuration for preprocessing
RESIZE_SHAPE = (128, 128)
SCALING = 1/255.

# Configuration for training
EPOCHS = 200
BATCH_SIZE = 96
LEARNING_RATE = 1e-3
NUM_STEPS_PER_EPOCH = 2000
SHUFFLE = True
MIN_LEARNING_RATE = 1e-6
COSINE_DECAY_LR_STEPS = 200
PATIENCE_EARLY_STOPPING = 20

# Configuration for evaluation
# None if all batches should be used
NUM_BATCHES_FOR_EVALUATION = None

# Configuration for model checkpoint
MODEL_CHECKPOINT_NAME = "resnet18_jigsaw_puzzle"
